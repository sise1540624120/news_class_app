import Vue from 'vue'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import App from './App.vue'
import VueRouter from "vue-router"
import top from "./components/top.vue"
import VueResource from "vue-resource"
//这里是自行添加组件
import home from './components/home.vue'

import xiala from './components/xiala.vue'
import tabbar from './components/tabbar.vue'
import search from './components/search.vue'
import swipe from './components/swipe.vue'
import Hcontent from './components/Hcontent.vue'
import Lcontent from './components/Lcontent.vue'
import near from './components/near.vue'
import my from './components/my.vue'
import like from './components/like.vue'
import field from './components/field.vue'
import login from './components/login.vue'
import carnews from './components/carnews.vue'
import hotnews from './components/hotnews.vue'
import gamenews from './components/gamenews.vue'
import swipe1 from './components/swipe1.vue'
import swipe2 from './components/swipe2.vue'
import swipe3 from './components/swipe3.vue'

// import writing from './components/writing.vue'
import store from './store.js'

Vue.config.debug = true;
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(MintUI)

// 配置路由
const router = new VueRouter({

    routes: [
        {
            path: '/',
            components: { top: top },
            children:[

                {path:'home',component:home},
                {path:'like',component:like},
                {path:'login',component:login},
                {path:'near',component:near},
                {path:'my',component:my},
                {path:"field",component:field},
                {path:"search",component:search},
                {path:"carnews",component:carnews},
                {path:"hotnews",component:hotnews},
                {path:"gamenews",component:gamenews},
                {path:"swipe1",component:swipe1},
                {path:"swipe2",component:swipe2},
                {path:"swipe3",component:swipe3},

                // {path:'proMsg',component:proMsg}
            ]
        }

    ]
})

// 挂载到根元素
const app = new Vue({
    store,
    router: router,
    render: h => h(App)
}).$mount('#app')
